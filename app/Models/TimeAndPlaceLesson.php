<?php

namespace App\Models;

use App\Services\StudyYearService;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeAndPlaceLesson extends Model
{
    use HasFactory;
    use HasUuids;
    protected $fillable = ['classroom', 'timeslot', 'lesson', 'group', 'subgroup', 'date'];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';

    /**
     * @param $entity = one of [group, classroom, teacher]
     * @param string $id - id of entity above
     * @return array|\Illuminate\Http\JsonResponse
     */

    public static function makeSchedule($entity, string $id)
    {
        $data = [];
        $data = (new StudyYearService())->timeSetter($data);
        $startWeek = $data['weekInfo'][0]['startWeek'];
        $endWeek = $data['weekInfo'][0]['endWeek'];

        $schedule = TimeAndPlaceLesson::query()
            ->where($entity, $id)
            ->whereBetween('date', [$startWeek, $endWeek])
            ->join('timeslots', 'time_and_place_lessons.timeslot', '=', 'timeslots.id')
            ->join('groups', 'time_and_place_lessons.group', '=', 'groups.id')
            ->join('classrooms', 'time_and_place_lessons.classroom', '=', 'classrooms.id')
            ->join('lessons', 'time_and_place_lessons.lesson', '=', 'lessons.id')
            ->join('subjects', 'lessons.subject', '=', 'subjects.id')
            ->join('teachers', 'lessons.teacher', '=', 'teachers.id')
            ->join('users', 'teachers.user_id', '=', 'users.id')
            ->select('time_and_place_lessons.id as lesson_id',
                'subjects.subject_name as subject_name',
                'lessons.type_of_lesson as type_of_lesson',
                'classrooms.classroom_name as classroom_name',
                'users.userName as teacher',
                'groups.group_number as group_number',
                'time_and_place_lessons.subgroup as subgroup',
                'timeslots.timeslot_number as timeslot_number',
                'time_and_place_lessons.date as date'
            )
            ->orderBy('date', 'asc')
            ->orderBy('timeslot_number', 'asc')
            ->get();

        return $schedule;
    }
}

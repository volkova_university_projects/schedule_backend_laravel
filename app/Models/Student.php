<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    use HasUuids;
    protected $fillable = ['group', 'subgroup', 'user_id'];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
}

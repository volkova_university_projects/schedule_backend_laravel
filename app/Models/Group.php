<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    use HasUuids;
    protected $fillable = ['group_number', 'capacity'];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
}

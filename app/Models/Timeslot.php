<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timeslot extends Model
{
    use HasFactory;
    use HasUuids;
    protected $fillable = ['timeslot_number', 'time_start', 'time_end'];

    protected $hidden = ['id', 'created_at', 'updated_at'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
}

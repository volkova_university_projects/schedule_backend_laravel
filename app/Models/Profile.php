<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['userName', 'group', 'email', 'password'];

    protected $hidden = ['id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
}

<?php

namespace App\Services;

use Illuminate\Support\Carbon;

class StudyYearService
{
    public function timeSetter($data) {
        $studyYearStart = Carbon::parse('first day of September');
        $timeNow = Carbon::now();
        if ($studyYearStart->diffInSeconds($timeNow, false) < 0) {
            $studyYearStart->subYear()->startOfWeek();
        }

        if (!empty(request()->query('weekStart')) && !empty(request()->query('weekEnd'))) {
            $startWeek = request()->query('weekStart');
            $endWeek = request()->query('weekEnd');

            if (!(Carbon::parse($startWeek)->isMonday() &&
                Carbon::parse($endWeek)->isSunday() &&
                Carbon::parse($startWeek)->diffInDays($endWeek) == 6)) {
                    $startWeek = $timeNow->startOfWeek()->format('Y-m-d');
                    $endWeek = $timeNow->endOfWeek()->format('Y-m-d');
            }
        } else {
            $startWeek = $timeNow->startOfWeek()->format('Y-m-d');
            $endWeek = $timeNow->endOfWeek()->format('Y-m-d');
        }
        $currentWeek = $studyYearStart->diffInWeeks(Carbon::parse($startWeek)->addWeek());

        $data['weekInfo'][] = [
            'startWeek' => $startWeek,
            'endWeek' => $endWeek,
            'currentWeek' => $currentWeek
        ];
        return $data;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ScheduleElementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'lesson_id' => $this->lesson_id,
            'lesson' => $this->subject_name,
            'type_of_lesson' => $this->type_of_lesson,
            'classroom' => $this->classroom_name,
            'teacher' => $this->teacher,
            'group' => $this->group_number,
            'subgroup' => $this->subgroup,
            'timeslot' => $this->timeslot_number,
            'date' => $this->date
        ];
    }
}

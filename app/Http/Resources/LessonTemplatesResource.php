<?php

namespace App\Http\Resources;

use App\Models\Lesson;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class LessonTemplatesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $lessons = Lesson::query()->where('id', '=', $this->id);
        $subject = Subject::query()->where('id', '=', $lessons->value('subject'));
        $teacher = Teacher::query()->where('id', '=', $lessons->value('teacher'));
        $teacher_name = User::query()->where('id', '=', $teacher->value('user_id'));

        return [
            'template_id' => $lessons->value('id'),
            'subject' => $subject->value('subject_name'),
            'teacher' => $teacher_name->value('userName'),
            'type_of_lesson' => $lessons->value('type_of_lesson')
        ];
    }
}

<?php

namespace App\Http\Resources;

use App\Models\Classroom;
use App\Models\Group;
use App\Models\GroupOnLesson;
use App\Models\Lesson;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\TimeAndPlaceLesson;
use App\Models\Timeslot;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ScheduleLessonsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $place_lesson = TimeAndPlaceLesson::query()
            ->where('id', '=', $this->id);
        $lesson = Lesson::query()
            ->where('id', '=', $place_lesson->value('lesson'));
        $subject = Subject::query()
            ->where('id', '=', $lesson->value('subject'));
        $teacher_in_lesson = Teacher::query()
            ->where('id', '=', $lesson->value('teacher'));
        $teacher_user = User::query()
            ->where('id', '=', $teacher_in_lesson->value('user_id'));
        //$group_on_lesson = group_on_lesson::query()
         //   ->where('id', '=', $place_lesson->value('groups_on_lesson'));
        $group = Group::query()
            ->where('id', '=', $place_lesson->value('group'));
        $classroom = Classroom::query()
            ->where('id', '=', $place_lesson->value('classroom'));
        $timeslot = Timeslot::query()
            ->where('id', '=', $place_lesson->value('timeslot'));

        return [
            'lesson_id' => $place_lesson->value('id'),
            'teacher' => $teacher_user->value('userName'),
            'subject' => $subject->value('subject_name'),
            'type_of_lesson' => $lesson->value('type_of_lesson'),
            'group' => $group->value('group_number'),
            'subgroup' => $place_lesson->value('subgroup'),
            'classroom' => $classroom->value('classroom_name'),
            'date' => $place_lesson->value('date'),
            'timeslot' => $timeslot->value('timeslot_number'),
        ];

    }
}

<?php

namespace App\Http\Resources;

use App\Models\Group;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     *
     *
     */

    public function toArray($request)
    {
        $current_student = Student::query()->where('user_id', '=', $this->id);
        $current_user = User::query()->where('id', '=', $this->id);
        $current_group = Group::query()->where('id', '=', $current_student->value('group'));

        return [
            'id' => $this->id,
            'userName' => $current_user->value('userName'),
            'group' => $current_group->value('group_number'),
            'email' => $current_user->value('email'),
            'is_student' => $current_user->first()->isStudent(),
            'is_teacher' => $current_user->first()->isTeacher(),
            'is_scheduler' => $current_user->first()->isScheduler()
        ];
    }
}

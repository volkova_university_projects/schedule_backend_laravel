<?php

namespace App\Http\Resources;

use App\Services\StudyYearService;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ScheduleOnWeekCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $coll = ScheduleElementResource::collection($this);

        return [
            "weekInfo" => (new StudyYearService())->timeSetter([]),
            "schedule" => $coll->groupBy(['date','timeslot_number'])->all()
        ];

    }
}

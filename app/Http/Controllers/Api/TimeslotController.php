<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Timeslot;
use Illuminate\Http\JsonResponse;

class TimeslotController extends Controller
{
    public function index() : JsonResponse {
        if(empty(request()->filter)) {
            $timeslots = Timeslot::query()
                ->select(
                    'timeslots.id as timeslot_id',
                    'timeslots.timeslot_number as timeslotNumber',
                    'timeslots.time_start as startLesson',
                    'timeslots.time_end as endLesson'
                )->get();
        } else {
            $timeslots = Timeslot::query()
                ->where('timeslots.time_start', 'ilike', ('%' . request()->filter . '%'))
                ->orWhere('timeslots.time_end', 'ilike', ('%' . request()->filter . '%'))
                ->select(
                    'timeslots.id as timeslot_id',
                            'timeslots.timeslot_number as timeslotNumber',
                            'timeslots.time_start as startLesson',
                            'timeslots.time_end as endLesson'
                )->get();
        }
        return response()->json($timeslots);

    }
}

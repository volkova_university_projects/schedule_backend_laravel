<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Scheduler;
use App\Services\PaginationService;

class SchedulerController extends Controller
{
    public function index() {
            if (empty(request()->filter)) {
                $schedulers = Scheduler::query()
                    ->join('users', 'schedulers.user_id', '=', 'users.id')
                    ->select(
                        'schedulers.id as scheduler_id',
                        'users.userName as userName'
                    )
                    ->get();
            } else {
                $schedulers = Scheduler::query()
                    ->join('users', 'schedulers.user_id', '=', 'users.id')
                    ->where('users.userName', 'ilike', ('%' . request()->filter . '%'))
                    ->select(
                        'schedulers.id as scheduler_id',
                                'users.userName as userName')
                    ->get();

            }
        $data = $schedulers;
        return (new PaginationService())->pagination($data, 'schedulers');
    }
}

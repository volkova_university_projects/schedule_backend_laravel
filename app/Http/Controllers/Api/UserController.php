<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Scheduler;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use App\Services\PaginationService;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index() {

        if(empty(request()->filter)) {
            $users = User::query()
                ->select(
                    'users.id as user_id',
                            'users.userName as userName'
                )->get();
        } else {
            $users = User::query()
                ->where('users.userName', 'ilike', ('%' . request()->filter . '%'))
                ->select(
                    'users.id as user_id',
                    'users.userName as userName'
                )->get();
        }
            $data = [];
            foreach ($users as $user) {
                $roles = [];

                $student = Student::query()->where('user_id', $user->user_id)->first();
                $scheduler = Scheduler::query()->where('user_id', $user->user_id)->first();
                $teacher = Teacher::query()->where('user_id', $user->user_id)->first();

                if ($student) $roles[] = 'student';
                if ($scheduler) $roles[] = 'scheduler';
                if ($teacher) $roles[] = 'teacher';

                $data[] = [
                    'user_id' => $user->user_id,
                    'userName' => $user->userName,
                    'roles' => $roles
                ];
            }

            return (new PaginationService())->pagination($data, 'users');
    }


    public function create() {
        $data = request();

        if ($data->groupNumber == null) {
            $group = null;
        } else {
            if (!Group::query()->where('group_number', $data->groupNumber)->exists()) {
                return response()->json([
                    'message' => 'Group with such number cannot be reached throughout database and intergalactic queries'
                ], 404);
            }
            $group = Group::query()->where('group_number', $data->groupNumber)->first()->id;
        }

        $validator = Validator::make($data->all(), [
            'userName' => 'required|string|max:255',
            'authData.email' => 'required|email|max:255',
            'authData.password' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 401);
        } else {
            $id = User::query()->create([
                'userName' => $data->userName,
                'email' => $data->authData['email'],
                'password' => Hash::make($data->authData['password'])
            ])->id;

            Student::query()->create([
                'user_id' => $id,
                'group' => $group
            ]);

            return response()->json([
                'status' => true,
                'message' => 'User successfully created'
            ], 200);
        }
    }

    public function update(string $id) {
        $data = request();

        if ($data->groupNumber == null) {
            $group = null;
        } else {
            if (!Group::query()->where('group_number', $data->groupNumber)->exists()) {
                return response()->json([
                    'message' => 'Group with such number cannot be reached throughout database and intergalactic queries'
                ], 404);
            }
            $group = Group::query()->where('group_number', $data->groupNumber)->first()->id;
        }

        $validator = Validator::make($data->all(), [
            'roles' => 'nullable|array',
            'userName' => 'required|string|max:255',
            'authData.email' => 'required|email|max:255',
            'authData.password' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 401);
        } else {
            User::query()->find($id)->update([
                'userName' => $data->userName,
                'email' => $data->authData['email'],
                'password' => Hash::make($data->authData['password'])
            ]);
            $allRoles = ['student', 'scheduler', 'teacher'];
            $roles = $data->roles;
            foreach ($allRoles as $role) {
                if (in_array($role, $roles)) {
                    switch ($role) {
                        case 'student':
                            if ($data->groupNumber == null) $group = null;
                            Student::query()->updateOrCreate([
                                'user_id' => $id,
                                'group' => $group
                            ]);
                            break;
                        case 'scheduler':
                            Scheduler::query()->updateOrCreate([
                                'user_id' => $id,
                            ]);
                            break;
                        case 'teacher':
                            Teacher::query()->updateOrCreate([
                                'user_id' => $id,
                            ]);
                            break;
                        default:
                            echo 'Can\'t create a role';
                            break;
                    }
                } else {
                    switch ($role) {
                        case 'student':
                            $exists = Student::query()->where('user_id', $id)->first();
                            $exists?->delete();
                            break;
                        case 'scheduler':
                            $exists = Scheduler::query()->where('user_id', $id)->first();
                            $exists?->delete();
                            break;
                        case 'teacher':
                            $exists = Teacher::query()->where('user_id', $id)->first();
                            $exists?->delete();
                            break;
                        default:
                            echo 'Can\'t delete a role';
                            break;
                    }
                }
            }

            return response()->json([
                'status' => true,
                'message' => 'User successfully updated'
            ], 200);
        }
    }

    public function destroy($id) {
        try {
            User::query()->findOrFail($id);
        } catch (Exception) {
            return response()->json([
                'status' => false,
                'message' => 'User doesn\'t exist'
            ], 404);
        }

        User::destroy($id);
        return response()->json([
            'status' => true,
            'message' => 'User successfully deleted'
        ], 200);
    }
}

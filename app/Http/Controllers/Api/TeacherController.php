<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ScheduleOnWeekCollection;
use App\Models\Teacher;
use App\Models\TimeAndPlaceLesson;
use App\Services\PaginationService;
use Symfony\Component\HttpFoundation\JsonResponse;

class TeacherController extends Controller
{
    public function index(): JsonResponse
    {
        if (empty(request()->filter)) {
            $teachers = Teacher::query()
                ->join('users', 'teachers.user_id', '=', 'users.id')
                ->select(
                    'teachers.id as teacher_id',
                    'users.userName as userName'
                )
                ->get();
        } else {
            $teachers = Teacher::query()
                ->join('users', 'teachers.user_id', '=', 'users.id')
                ->where('users.userName', 'ilike', ('%' . request()->filter . '%'))
                ->select(
                    'teachers.id as teacher_id',
                    'users.userName as userName')
                ->get();

        }
        $data = $teachers;
        return (new PaginationService())->pagination($data, 'teachers');


//        try {
//            $data = [];
//            $teachers = Teacher::all();
//            foreach ($teachers as $teacher) {
//                $userName = User::query()->where('id', $teacher->user_id)->first()->userName;
//                $data[] = [
//                    'id' => $teacher->id,
//                    'userName' => $userName,
//                ];
//            }
//            return (new PaginationService())->pagination($data, 'teachers');
//        } catch (Exception) {
//            return response()->json([
//                'status' => false,
//                'message' => 'An error has occurred'
//            ], 400);
//        }
    }

    public function indexSchedule(string $id) {
        if (empty(Teacher::query()->find($id)->id)) {
            return response()->json([
                'status' => false,
                'message' => 'This teacher doesn\'t exist'
            ], 404);
        }
        $data = TimeAndPlaceLesson::makeSchedule('teacher', $id);

        return response()->json(new ScheduleOnWeekCollection($data));
    }
}

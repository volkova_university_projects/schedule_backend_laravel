<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\LessonTemplatesCollection;
use App\Models\Lesson;
use App\Models\Subject;
use App\Models\Teacher;
use App\Services\PaginationService;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LessonTemplatesController extends Controller {

    public function index()
    {
        if(empty(request()->filter)) {
            $query = Lesson::all();
            $templates = new LessonTemplatesCollection($query);
            $data = $templates;
            return (new PaginationService())->pagination($data, 'templates');
        } else {
            $query = Lesson::query()
                ->join('subjects', 'lessons.subject', '=', 'subjects.id')
                ->join('teachers', 'lessons.teacher', '=', 'teachers.id')
                ->join('users', 'teachers.user_id', '=', 'users.id')
                ->where('subjects.subject_name', 'ilike', ('%' . request()->filter . '%'))
                ->orWhere('users.userName', 'ilike', ('%' . request()->filter . '%'))
                ->orWhere('lessons.type_of_lesson', 'ilike', ('%' . request()->filter . '%'))
                ->select(
                    'lessons.id as template_id',
                            'subjects.subject_name as subject',
                            'users.userName as teacher',
                            'lessons.type_of_lesson as type_of_lesson'
                )
                ->get();

            $data = $query;
            return (new PaginationService())->pagination($data, 'templates');
        }
    }

    public function createTemplate(Request $request)
    {
        $data = request();

        $validateTemplate = Validator::make($data->all(),
            [
                'teacher' => 'required|uuid',
                'subject' => 'required|uuid',
                'type_of_lesson' => 'required|string'
            ]);

            if($validateTemplate->fails()) {
                return response()->json([
                    'message' => $validateTemplate->errors()
                ], 400);
            } else {
                if(Lesson::query()->where([
                    'teacher' => $data->teacher,
                    'subject' => $data->subject,
                    'type_of_lesson' => $data->type_of_lesson
                ])->exists()){
                    return response()->json([
                        'message' => "Duplicate template"
                    ], 409);
                } else {
                    $id = Lesson::create([
                        'teacher' => $data->teacher,
                        'subject' => $data->subject,
                        'type_of_lesson' => $data->type_of_lesson
                    ])->id;
                    return response()->json([
                        'massage' => $id
                    ], 201);
                }
            }
    }

    public function updateTemplate(Request $request)
    {
        $data = request();

        $validateTemplate = Validator::make($data->all(),
            [
                'teacher' => 'nullable|uuid',
                'subject' => 'nullable|uuid',
                'type_of_lesson' => 'nullable|string'
            ]);

        $current_template = Lesson::query()
            ->where('id','=', $request->id)->get();

        if(!$current_template) {
            return response()->json([
                'message' => "No such lesson's template"
            ], 404);
        }
        $current_template = $current_template->firstOrFail();

        if($validateTemplate->fails()) {
            return response()->json([
                'message' => $validateTemplate->errors()
            ],400);
        }

        if(!empty($data->subject)) {
            $subject = Subject::query()
                ->where('id', '=', $data->subject)
                ->get() //workaround for non-changing entity!
                ->value('id');
            $current_template->subject = $subject;
        }

        if(!empty($data->teacher)) {
            $teacher = Teacher::query()
                ->where('id', '=', $data->teacher)
                ->get()
                ->value('id');
            $current_template->teacher = $teacher;
        }

        if(!empty($data->type_of_lesson)) {
            $current_template->type_of_lesson = $data->type_of_lesson;
        }

        if($current_template->isDirty()) {
            $current_template->save();
            return response()->json([
                'message' => 'Lesson\'s template was changed'
            ],200);
        } else {
            return response()->json([
                'message' => 'No changes'
            ], 200);
        }
    }

    public function deleteTemplate(Request $request)
    {
        try {
            $lesson = Lesson::query()->where('id', '=', $request->id)->firstOrFail();
        } catch (Exception $exception) {
            return response()->json([
                'message' => 'Template doesn\'t exist'
            ], 404);
        }
        Lesson::query()
            ->where('id', '=', $request->id)
            ->delete();
        return response()->noContent(204, ['Content-Type' => 'text/plain']);
    }
}

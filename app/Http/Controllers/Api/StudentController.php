<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Services\PaginationService;

class StudentController extends Controller
{
    public function index() {

        if(empty(request()->filter)) {
            $students = Student::query()
                ->join('users', 'students.user_id', '=', 'users.id')
                ->join('groups', 'students.group', '=', 'groups.id')
                ->select(
                    'students.id as student_id',
                    'users.userName as userName',
                    'groups.group_number as group',
                    'students.subgroup as subgroup'
                )
                ->get();
        } else {
            $students = Student::query()
                ->join('users', 'students.user_id', '=', 'users.id')
                ->join('groups', 'students.group', '=', 'groups.id')
                ->where('users.userName', 'ilike', ('%' . request()->filter . '%'))
                ->orWhere('groups.group_number', 'ilike', ('%' . request()->filter . '%'))
                ->select(
                    'students.id as student_id',
                            'users.userName as userName',
                            'groups.group_number as group',
                            'students.subgroup as subgroup'
                )
                ->get();
        }
        $data = $students;
        return (new PaginationService())->pagination($data, 'students');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ScheduleLessonsResource;
use App\Http\Resources\ScheduleLessonsCollection;
use App\Models\Classroom;
use App\Models\Group;
use App\Models\Lesson;
use App\Models\TimeAndPlaceLesson;
use App\Models\Timeslot;
use App\Services\PaginationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ScheduleLessonsController extends Controller
{
    public function index(Request $request, $page = 1)
    {
        if(empty(request()->filter)) {
            $query = TimeAndPlaceLesson::all();
            $lessons = new ScheduleLessonsCollection($query);
            $data = $lessons;
        } else {
            $query = TimeAndPlaceLesson::query()
                ->join('lessons', 'time_and_place_lessons.lesson', '=', 'lessons.id')
                ->join('teachers', 'lessons.teacher', '=', 'teachers.id')
                ->join('subjects', 'lessons.subject', '=', 'subjects.id')
                ->join('classrooms', 'time_and_place_lessons.classroom', '=', 'classrooms.id')
                ->join('groups', 'time_and_place_lessons.group', '=', 'groups.id')
                ->join('timeslots', 'time_and_place_lessons.timeslot', '=', 'timeslots.id')
                ->join('users', 'teachers.user_id', '=', 'users.id')
                ->where('lessons.type_of_lesson', 'ilike', ('%' . request()->filter . '%'))
                ->orWhere('users.userName', 'ilike', ('%' . request()->filter . '%'))
                ->orWhere('subjects.subject_name', 'ilike', ('%' . request()->filter . '%'))
                ->orWhere('classrooms.classroom_name', 'ilike', ('%' . request()->filter . '%'))
                ->orWhere('groups.group_number', 'ilike', ('%' . request()->filter . '%'))
                ->orWhere('time_and_place_lessons.date', 'ilike', ('%' . request()->filter . '%'))
                ->select('time_and_place_lessons.id as lesson_id',
                                'users.userName as teacher',
                                'subjects.subject_name as subject',
                                'lessons.type_of_lesson as type_of_lesson',
                                'groups.group_number as group',
                                'time_and_place_lessons.subgroup as subgroup',
                                'classrooms.classroom_name as classroom',
                                'time_and_place_lessons.date as date',
                                'timeslots.timeslot_number as timeslot')
                ->get();
            $data = $query;
        }
        return (new PaginationService())->pagination($data, 'lessons');
    }


    public function create(Request $request)
    {
        $data = request();

        $validateLesson = Validator::make($data->all(),
            [
                'lesson' => 'required',
                'classroom' => 'required',
                'group' => 'required',
                'subgroup' => 'string|nullable',
                'timeslot' => 'required',
                'date' => 'required'
            ]);

            if($validateLesson->fails()) {
                return response()->json([
                   'message' => $validateLesson->errors()
                ], 400);
            } else {
                $classroom = Classroom::query()
                    ->where('id', '=', $data->classroom)->get()->value('id');
                $timeslot = Timeslot::query()
                    ->where('id', '=', $data->timeslot)->get()->value('id');
                $lesson = Lesson::query()
                    ->where('id', '=', $data->lesson)->get()->value('id');
                $group = Group::query()
                    ->where('id', '=', $data->group)->get()->value('id');

                if (TimeAndPlaceLesson::query()->where([
                    'lesson' => $lesson,
                    'classroom' => $classroom,
                    'group' => $group,
                    'subgroup' => $data->subgroup,
                    'timeslot' => $timeslot,
                    'date' => $data->date
                ])->exists()){
                    return response()->json([
                        'status' => false,
                        'message' => "Duplicate lesson"
                    ], 409);

                } else {

                    $id = TimeAndPlaceLesson::create([
                        'lesson' => $lesson,
                        'classroom' => $classroom,
                        'group' => $data->group,
                        'subgroup' => $data->subgroup,
                        'timeslot' => $timeslot,
                        'date' => $data->date
                    ])->id;
                    return response()->json([
                        'status' => true,
                        'message' => $id
                    ], 201);
                }
            }

    }

    public function updateLesson(Request $request)
    {
        $data = request();

        $validateLesson = Validator::make($data->all(),
            [
                'lesson' => 'uuid',
                'classroom' => 'uuid',
                'group' => 'uuid',
                'subgroup' => 'string',
                'timeslot' => 'uuid',
                'date' => 'date'
            ]);
        $current_lesson = TimeAndPlaceLesson::query()
            ->where('id','=',$request->id)->first();
        if (!$current_lesson){
            return response()->json([
                'message' => "No such lesson in schedule"
            ], 404);
        }


        if($validateLesson->fails()) {
            return response()->json([
                'message' => $validateLesson->errors()
            ], 400);
        } else {
            if (!empty($data->classroom)) {
                $classroom = Classroom::query()
                    ->where('id', '=', $data->classroom)->get()->value('id');
                $current_lesson->classroom = $classroom;
            }
            if (!empty($data->timeslot)) {
                $timeslot = Timeslot::query()
                    ->where('id', '=', $data->timeslot)->get()->value('id');
                $current_lesson->timeslot = $timeslot;
            }
            if (!empty($data->lesson)) {
                $lesson = Lesson::query()
                    ->where('id', '=', $data->lesson)->get()->value('id');
                $current_lesson->lesson = $lesson;
            }
            if (!empty($data->group)) {
                $group = Group::query()
                    ->where('id', '=', $data->group)->get()->value('id');
                $current_lesson->group = $group;
            }

            if (!empty($data->date)) {
                $current_lesson->date = $data->date;
            }

            $current_lesson->save();
                return response()->json([
                    'message' => $current_lesson->wasChanged(),
                ], 200);

        }

    }

    public function showLesson(Request $request)
    {
        try {
            $lesson = TimeAndPlaceLesson::findOrFail($request);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Lesson doesn\'t exist'
            ], 404);
        }

        return response()->json(new ScheduleLessonsResource(
            TimeAndPlaceLesson::query()
            ->where('id', '=', $request->id)->first()), 200);

    }

    public function deleteLesson(Request $request)
    {
        try {
            $lesson = TimeAndPlaceLesson::findOrFail($request);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Lesson doesn\'t exist'
            ], 404);
        }
        TimeAndPlaceLesson::query()
            ->where('id', '=', $request->id)
            ->delete();
        return response()->noContent(204, ['Content-Type' => 'text/plain']);
    }
}

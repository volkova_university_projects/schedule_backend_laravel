<?php


namespace App\Http\Controllers\Api;

use App\Models\Profile;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;



class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['loginUser','createUser']]);
    }

    public function createUser(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(),
                [
                    'userName' => 'required',
                    'email' => 'required|email|unique:App\Models\User,email',
                    'password' => 'required'
                ]);

            if($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $user = new User([
                'userName' => $request->userName,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            $user->save();

            $student = new Student(['user_id'=>$user->id]);
            $student->save();


            //выписать токен при регистрации
            $credentials = $request->only('email', 'password');
            $token = Auth::attempt($credentials);
            $user = Auth::user();
            return response()->json([
                'token'=> $token
            ], 200);



        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function loginUser(Request $request)
    {
        try { //проверяем заполнены ли поля, иначе 400 статус
            $validateUser = Validator::make($request->all(),
                [
                    'email' => 'required',
                    'password' => 'required'
                ]);

            if($validateUser->fails()){
                return response()->noContent(400);
            }

            $credentials = $request->only('email', 'password');
            // пытаемся войти, если не удалось http 400
            $token = Auth::attempt($credentials);
            if (!$token) {
                return response()->json([
                    'message' => 'Login failed',
                ], 400);
            }
            // возвращаем токен JWT
            $user = Auth::user();
            return response()->json([
                'token' => $token,
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 500);
        }
    }



    public function logoutUser(Request $request) {
        Auth::logout();
        return response()->noContent(200);
    }

    public function refresh()
    {
        return response()->json([
            'token' => Auth::refresh()
        ]);
    }
};



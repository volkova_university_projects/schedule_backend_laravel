<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ScheduleOnWeekCollection;
use App\Models\Classroom;
use App\Models\TimeAndPlaceLesson;
use App\Services\PaginationService;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;

class ClassroomController extends Controller
{
    public function index() {

        if(empty(request()->filter)) {
            $classrooms = Classroom::query()
                ->select(
                    'classrooms.id as classroom_id',
                            'classrooms.classroom_name as classroomName'
                )->get();
        } else {
            $classrooms = Classroom::query()
                ->where('classrooms.classroom_name', 'ilike', ('%' . request()->filter . '%'))
                ->select(
                    'classrooms.id as classroom_id',
                            'classrooms.classroom_name as classroomName'
                )->get();
        }
        $data = $classrooms;
            return (new PaginationService())->pagination($data, 'classrooms');
    }

    public function create() {
        $classroom = request();
        $validator = Validator::make($classroom->all(), [
            'classroomName' => 'required|string|max:255',
            'capacity' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 401);
        } else {
            Classroom::query()->create([
                'classroom_name' => $classroom->input('classroomName'),
                'capacity' => $classroom->input('capacity')
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Classroom successfully created'
            ], 200);
        }
    }

    public function update(string $id) {
        $classroom = request();
        $validator = Validator::make($classroom->all(), [
            'classroomName' => 'required|string|max:255',
            'capacity' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 401);
        } else {
            Classroom::query()->find($id)->update([
                'classroom_name' => $classroom->input('classroomName'),
                'capacity' => $classroom->input('capacity')
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Classroom successfully updated'
            ], 200);
        }
    }

    public function destroy(string $id) {
        try {
            $user = Classroom::query()->findOrFail($id);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => 'Classroom doesn\'t exist'
            ], 404);
        }

        Classroom::destroy($id);
        return response()->json([
            'status' => true,
            'message' => 'Classroom successfully deleted'
        ], 200);
    }

    public function indexSchedule(string $id) {

        if (empty(Classroom::query()->find($id)->id)) {
            return response()->json([
                'status' => false,
                'message' => 'This classroom doesn\'t exist'
            ], 400);
        }

        $data = TimeAndPlaceLesson::makeSchedule('classroom', $id);

        return response()->json(new ScheduleOnWeekCollection($data));
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\ProfileResource;
use App\Models\Group;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Show the profile of current user.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function show(Request $request) {
        return response()->json(new ProfileResource(
            User::query()->where('id','=', $request->user()->id)->first()

    ));
    }

//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  \App\Models\Profile  $profile
//     * @return \Illuminate\Http\Response
//     */
//    public function edit(Profile $profile)
//    {
//        //
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request)
    {
        $current_user = User::query()->where('id','=', $request->user()->id)->first();
        $is_student = $current_user->isStudent();

        if (!empty($request->email)) {
            $current_user->email = $request->email;
        }
        if (!empty($request->password)) {
            $current_user->password = Hash::make($request->password);
        }
        if (!empty($request->userName)) {
                $current_user->userName = $request->userName;
        }
        if($current_user->isDirty()) {
            $current_user->save();
        }

        if ($is_student && !empty($request->group)) {
            $student = Student::query()
                ->where('user_id', '=', $current_user->id)
                ->first();
            if (Group::query()->where('group_number', '=', $request->group)->exists()) {
                $student->group = Group::query()
                    ->where('group_number', '=', $request->group)
                    ->first()->id;
                $student -> save();
            } else {
                return response()
                    ->json([
                        'message' => 'Group with such number cannot be reached through out database and intergalactic queries'
                    ], 404);
            }
        }

        return response()
            ->noContent(204)
            ->withHeaders([
                'Content-Type' => 'text/plain'
            ]);
    }

}

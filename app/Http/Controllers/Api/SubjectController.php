<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use App\Services\PaginationService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class SubjectController extends Controller
{
    public function index() : JsonResponse {

        if(empty(request()->filter)) {
            $subjects = Subject::query()
                ->select(
                    'subjects.id as subject_id',
                    'subjects.subject_name as subject'
                )
                ->get();
        } else {
            $subjects = Subject::query()
                ->where('subject_name', 'ilike', ('%' . request()->filter . '%'))
                ->select(
                    'subjects.id as subject_id',
                            'subjects.subject_name as subject'
                )
                ->get();
            }
        $data = $subjects;
        return (new PaginationService())->pagination($data, 'lesson_templates');
    }

    public function create() {
        $data = request();

        $validator = Validator::make($data->all(), [
            'subject_name' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            Subject::query()->create([
                'subject_name' => $data->input('subject_name')
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Subject was successfully created'
            ], 200);
        }
    }

    public function update(string $id) {
        if (!Subject::query()->find($id)) {
            return response()->json([
                'status' => 'false',
                'message' => 'Subject doesn\'t exist'
            ], 404);
        }

        $data = request();

        $validator = Validator::make($data->all(), [
            'subject_name' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            Subject::query()->find($id)->update([
                'subject_name' => $data->input('subject_name')
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Group data was updated'
            ], 200);
        }
    }

    public function destroy(string $id) {
        try {
            Subject::query()->findOrFail($id);

        } catch (Exception) {
            return response()->json([
                'status' => false,
                'message' => 'Subject doesn\'t exist'
            ], 404);
        }

        Subject::destroy($id);
        return response()->json([
            'status' => true,
            'message' => 'Subject was successfully deleted'
        ], 200);
    }
}

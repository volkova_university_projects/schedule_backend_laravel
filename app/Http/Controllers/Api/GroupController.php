<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ScheduleOnWeekCollection;
use App\Models\Group;
use App\Models\Scheduler;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\TimeAndPlaceLesson;
use App\Models\User;
use App\Services\PaginationService;
use Exception;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;

class GroupController extends Controller
{
    public function index() {
        if(empty(request()->filter)) {
            $groups = Group::query()
                ->select(
                    'groups.id as group_id',
                            'groups.group_number as groupNumber'
                )->get();
        } else {
            $groups = Group::query()
                ->where('groups.group_number', 'ilike', ('%' . request()->filter . '%'))
                ->select(
                    'groups.id as group_id',
                    'groups.group_number as groupNumber'
                )->get();
        }
        $data = $groups;
        return (new PaginationService())->pagination($data, 'groups');
    }


    public function create() {
        $data = request();

        $validator = Validator::make($data->all(), [
            'groupNumber' => 'required|integer',
            'capacity' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 400);
        } else {
            Group::query()->create([
                'group_number' => $data->input('groupNumber'),
                'capacity' => $data->input('capacity')
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Group was successfully created'
            ], 200);
        }
    }

    public function update(string $id) {
        if (!Group::query()->find($id)) {
            return response()->json([
                'status' => 'false',
                'message' => 'Group doesn\'t exist'
            ], 404);
        }

        $data = request();

        $validator = Validator::make($data->all(), [
            'groupNumber' => 'required|integer',
            'capacity' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ], 401);
        } else {
            Group::query()->find($id)->update([
                'group_number' => $data->input('groupNumber'),
                'capacity' => $data->input('capacity')
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Group data was updated'
            ], 200);
        }
    }

    public function indexUsers(string $id) {
        if (!Group::query()->find($id)) {
            return response()->json([
                'status' => 'false',
                'message' => 'Group doesn\'t exist'
            ], 404);
        }

        $students = Student::all();
        $data = [];
        foreach ($students as $student) {
            $roles = [];
            if ($student->group == $id) {
                $roles[] = 'student';
                if (Scheduler::query()->where('user_id', $student->user_id)->first()) $roles[] = 'scheduler';
                if (Teacher::query()->where('user_id', $student->user_id)->first()) $roles[] = 'teacher';

                $userName = User::query()->where('id', $student->user_id)->first()->userName;

                $data['groupUsers'][] = [
                    'id' => $student->id,
                    'userName' => $userName,
                    'roles' => $roles
                ];
            }
        }
        return json_encode($data);
    }

    public function createUser(string $id) {
        if (!Group::query()->find($id)) {
            return response()->json([
                'status' => 'false',
                'message' => 'Group doesn\'t exist'
            ], 404);
        } else {
            $groupCap = Group::query()->find($id)->capacity;
        }

        if (!User::query()->find(request()->input('id'))) {
            return response()->json([
                'status' => 'false',
                'message' => 'User doesn\'t exist'
            ], 404);
        }

        $studentsNum = Student::query()
            ->where('group', $id)
            ->count();
        if ($groupCap == $studentsNum) {
            return response()->json([
                'status' => 'false',
                'message' => 'Group is full'
            ], 400);
        }

        $idUser = request()->input('id');
        $student = Student::query()
            ->where('user_id', $idUser)
            ->first();
        if (!$student) {
            return response()->json([
                'status' => false,
                'message' => 'This user is not a student'
            ], 404);
        } else {
            if ($student->group != null) {
                return response()->json([
                    'status' => 'false',
                    'message' => 'User is already in a group'
                ], 400);
            }
            $student->update(['group' => $id]);
            return response()->json([
                'status' => true,
                'message' => 'User successfully added to the group'
            ], 200);
        }
    }

    public function destroyUser(string $id, string $idUser) {
        try {
            User::query()->findOrFail($idUser);
            Group::query()->findOrFail($id);
        } catch (Exception) {
            return response()->json([
                'status' => false,
                'message' => 'User or group doesn\'t exist'
            ], 404);
        }
        Student::query()
            ->where('user_id', $idUser)
            ->update(['group' => null]);
        return response()->json([
            'status' => true,
            'message' => 'User successfully deleted from this group'
        ], 200);
    }

    public function indexSchedule(string $id) {

        if (empty(Group::query()->find($id)->id)) {
            return response()->json([
                'status' => false,
                'message' => 'This group doesn\'t exist'
            ], 404);
        }

        $data = TimeAndPlaceLesson::makeSchedule('group', $id);

        return response()->json(new ScheduleOnWeekCollection($data));
    }

}

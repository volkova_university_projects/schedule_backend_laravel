<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_and_place_lessons', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->foreignUuid('classroom')->references('id')->on('classrooms');
            $table->foreignUuid('timeslot')->references('id')->on('timeslots');
            $table->foreignUuid('lesson')->references('id')->on('lessons');
            $table->foreignUuid('group')->references('id')->on('groups');
            $table->string('subgroup')->nullable();
            $table->date('date');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE time_and_place_lessons ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_and_place_lessons');
    }
};

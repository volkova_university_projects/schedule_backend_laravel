<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_on_lessons', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->foreignUuid('group')->references('id')->on('groups');
            $table->foreignUuid('lesson')->references('id')->on('lessons');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE group_on_lessons ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_on_lessons');
    }
};

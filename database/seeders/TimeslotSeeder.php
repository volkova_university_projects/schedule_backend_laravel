<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimeslotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('timeslots')->insert(
            [
                ['id' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'timeslot_number' => 1, 'time_start' => '08:45:00', 'time_end' => '10:20:00'],
                ['id' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'timeslot_number' => 2, 'time_start' => '10:35:00', 'time_end' => '12:10:00'],
                ['id' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'timeslot_number' => 3, 'time_start' => '12:25:00', 'time_end' => '14:00:00'],
                ['id' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'timeslot_number' => 4, 'time_start' => '14:45:00', 'time_end' => '16:20:00'],
                ['id' => 'ce72d29e-ec13-4b05-8893-eac8d9f20269', 'timeslot_number' => 5, 'time_start' => '16:35:00', 'time_end' => '18:10:00'],
                ['id' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'timeslot_number' => 6, 'time_start' => '18:25:00', 'time_end' => '20:00:00'],
                ['id' => 'b8720d71-f98c-4abe-a835-090009a1d061', 'timeslot_number' => 7, 'time_start' => '20:15:00', 'time_end' => '21:50:00'],
            ]
        );
    }
}

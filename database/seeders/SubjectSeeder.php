<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert(
            [
                ['id' => 'e18a0ba9-192e-4eed-80b7-853a799f2276', 'subject_name' => 'Основы машинного обучения'],
                ['id' => '017b3252-30bc-4891-8602-84d8b38f8538', 'subject_name' => 'Требования'],
                ['id' => '8785dbab-11fb-407d-9082-932aae6a1edf', 'subject_name' => 'Математический анализ'],
                ['id' => 'a59beaec-070c-4c47-b8d9-8ed1c3af83e8', 'subject_name' => 'МКН'],
                ['id' => '92641936-83ee-4670-a0db-256b9fdd7b41', 'subject_name' => 'Иностранный язык'],
                ['id' => '5610a04c-26e0-4e12-a246-d1ba61381577', 'subject_name' => 'Философия'],
                ['id' => '80154d1a-f1c4-400d-b72b-c3f24a0cb18c', 'subject_name' => 'Тестирование'],
                ['id' => '3dad6253-1c83-49ff-b4b4-b160e352fb5e', 'subject_name' => 'ООП'],
                ['id' => '2527fb77-2a4d-42db-9ff4-a9d1b4d9b91e', 'subject_name' => 'Экономика'],
                ['id' => '51cbb285-e971-4ebe-9dbf-9b18734d1dcf', 'subject_name' => 'Риторика'],
                ['id' => '6f822ee0-2160-4fd9-93e9-f672806d4c72', 'subject_name' => 'Проектная деятельность'],
                ['id' => 'e9874376-c020-4075-b494-ccc71f6e3178', 'subject_name' => 'Машинное обучние'],
                ['id' => '0b26efc3-64ee-441d-9885-48a5bd73a1a2', 'subject_name' => 'Программирование'],
                ['id' => 'b50b5c89-3c39-450c-82c8-30b172859949', 'subject_name' => 'Базы данных'],
            ]
        );
    }
}

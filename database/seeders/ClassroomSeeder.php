<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classrooms')->insert(
            [
                ['id' =>'7c895e7e-d605-4ecc-970a-9094c1614689', 'classroom_name' => '101', 'capacity' => 30],
                ['id' =>'c7cd48f3-d2d6-4b98-8fea-43971b638afa', 'classroom_name' => '102', 'capacity' => 30],
                ['id' =>'2aff3fb1-bcd8-46f2-9a61-4e5eedb2a8cb', 'classroom_name' => '103', 'capacity' => 60],
                ['id' =>'9326569c-2050-49b2-a33a-d3f62d5aa398', 'classroom_name' => '104', 'capacity' => 60],
                ['id' =>'52724c3e-536b-4bbb-8c4b-61a176ca6af7', 'classroom_name' => '105', 'capacity' => 90],
                ['id' =>'030acae5-d27f-4f55-8dc9-d0ce794468ce', 'classroom_name' => '106', 'capacity' => 100],
                ['id' =>'143731e4-3ffe-4f3d-a87f-00e29f3f9a18', 'classroom_name' => '107', 'capacity' => 100],
                ['id' =>'71487dbb-b773-4c41-98f9-f1ee124a88da', 'classroom_name' => '108', 'capacity' => 20],
                ['id' =>'09d97959-056f-4293-83f3-2f67af262036', 'classroom_name' => '109', 'capacity' => 30],
                ['id' =>'fac2347c-8a5a-45c4-89f2-7d93b2b66ec6', 'classroom_name' => '201', 'capacity' => 30],
                ['id' =>'7efcecb6-43ef-42f0-b20a-b9a747738e7f', 'classroom_name' => '202', 'capacity' => 34],
                ['id' =>'72ded9e7-d10c-46e7-986d-2f4a2308cd89', 'classroom_name' => '203', 'capacity' => 37],
                ['id' =>'fb3214ce-4f92-4a3a-97e1-49a8f180c0f4', 'classroom_name' => '204', 'capacity' => 60],
                ['id' =>'559def39-80ae-41d7-b0ec-abeb756b1a3b', 'classroom_name' => '205', 'capacity' => 90],
                ['id' =>'4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'classroom_name' => '206', 'capacity' => 120],
                ['id' =>'fb84963e-6e3c-491d-a771-d1090d998241', 'classroom_name' => '207', 'capacity' => 200],
                ['id' =>'9317b00a-cd83-48ac-9526-8db7083a0c1e', 'classroom_name' => '208', 'capacity' => 20],
                ['id' =>'969acbbf-abbb-4e43-b614-9b35dd1d3fad', 'classroom_name' => '301', 'capacity' => 30],
                ['id' =>'0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'classroom_name' => '302', 'capacity' => 60],
                ['id' =>'e9ec9f81-2981-42a4-ba72-a2fbd8009688', 'classroom_name' => '303', 'capacity' => 60],
                ['id' =>'7199e78e-ad24-4873-b5ed-10dfe1d166a8', 'classroom_name' => '304', 'capacity' => 60],
                ['id' =>'9cc387ec-7dc1-49c2-bf26-bcc1670088e4', 'classroom_name' => '305', 'capacity' => 60],
                ['id' =>'afc8b4af-a772-4bb5-b407-3f4d08dff48b', 'classroom_name' => '306', 'capacity' => 90],
                ['id' =>'49e92242-177b-40d4-8cb4-6cb0874e75df', 'classroom_name' => '307', 'capacity' => 30],
            ]
        );
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupOnLessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('group_on_lessons')->insert(
            [
                ['id' => '5817a8f2-89c3-46b4-9026-1e8bb055aa6e', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c'],
                ['id' => '14f8507b-b38b-43b1-8fa4-547047adbb26', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c'],
                ['id' => 'd2a97814-18ec-45f1-85a1-f4326db25e66', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575'],
                ['id' => 'e624fe51-34d8-427a-94a5-109db28beca0', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a'],
                ['id' => '25fe48d3-1fd1-4f88-a866-30532d2d9211', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a'],
                ['id' => '67e7a642-0649-47c6-975b-e8bf4e6a8ec9', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'lesson' => 'adb5cccc-82a4-4013-904b-be11797bd872'],
                ['id' => 'c105b146-03fd-4995-b139-c11c13b762a3', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'lesson' => 'adb5cccc-82a4-4013-904b-be11797bd872'],
                ['id' => 'a43aa6b6-2654-46e7-87ba-7598c6c2c52a', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'lesson' => '562ee073-6161-4957-ba1a-3c508501d94c'],
                ['id' => '8f5c53a6-dab9-4437-a8ba-38bad2a46922', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c'],
                ['id' => '61458059-d9db-467e-93d8-7d62714dbdaa', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c'],
                ['id' => 'd936a797-502f-4ecb-bdbc-bdf3fdbaaaaf', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575'],
                ['id' => 'c35be6c5-6977-49b4-a58b-7b663e8ebdb7', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a'],
                ['id' => 'fc7ddf8e-5be3-49c7-be80-9eccb15e0736', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'lesson' => 'adb5cccc-82a4-4013-904b-be11797bd872'],
                ['id' => '06fc4dee-1ff9-4852-9add-575585a705dd', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'lesson' => 'adb5cccc-82a4-4013-904b-be11797bd872'],
                ['id' => 'f569b069-0d87-4d7e-90d3-df47ac356120', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'lesson' => '562ee073-6161-4957-ba1a-3c508501d94c'],
                ['id' => 'b941d614-dfd8-479e-870a-f1be90a61470', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'lesson' => 'a491a401-2f6a-45dd-9f09-4ae34878dac5'],
                ['id' => '07cb972b-423e-4d74-a2c3-3fc401a8f449', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'lesson' => 'cb1b8afa-a790-4dc4-b72f-ee233b2b53bb'],
                ['id' => 'd164fa1d-5516-410f-a923-b084dc711368', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c'],
                ['id' => 'e368e9e4-1baf-4922-a5ac-86acb1a68cf5', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c'],
                ['id' => '6ff4485d-b6ed-4b61-ab93-22b6d57c3802', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575'],
                ['id' => '5d5ba466-236c-4a6d-abed-6e7436bf36cf', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a'],
                ['id' => '3c319e95-ff6c-4aac-a0fd-d9984fd64fca', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'lesson' => 'adb5cccc-82a4-4013-904b-be11797bd872'],
                ['id' => '0e5763ad-d207-40c6-a333-ad41e23ee1b3', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'lesson' => 'adb5cccc-82a4-4013-904b-be11797bd872'],
                ['id' => '410b8c46-a92c-410a-a6e3-7983fce346ca', 'group' => 'f064c210-2673-4851-a2c7-e758bdf23239', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c'],
                ['id' => '55ad2b8d-8954-4c02-8cd5-a6c4c2f6d3ca', 'group' => 'f064c210-2673-4851-a2c7-e758bdf23239', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c'],
                ['id' => 'ceb99e76-abfa-42d9-ab0b-85c2ff39564b', 'group' => 'f064c210-2673-4851-a2c7-e758bdf23239', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575'],
                ['id' => 'b1f07d35-18d1-41a6-80b5-44d82da5f483', 'group' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'lesson' => '53e72adc-a886-4180-b208-5e5612e07447'],
                ['id' => '6d323f26-097f-41bc-bac2-a2dbdf6c9a84', 'group' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575'],
                ['id' => 'eba9a426-1676-44b6-b135-329200326dc2', 'group' => 'c12124f3-52a8-4429-bdd9-660c6df2eedc', 'lesson' => 'adb5cccc-82a4-4013-904b-be11797bd872'],
                ['id' => '89fc337d-4ffd-4526-bed6-39cce6f7262c', 'group' => 'c12124f3-52a8-4429-bdd9-660c6df2eedc', 'lesson' => 'e2fa57fc-182e-46c8-b8fa-f15f14c8dbd6'],
            ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert(
            [
                ['id' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'group_number' => 100001, 'capacity' => 30],
                ['id' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'group_number' => 100002, 'capacity' => 30],
                ['id' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'group_number' => 100003, 'capacity' => 30],
                ['id' => 'c9802e13-57c2-4322-acbe-278050376b46', 'group_number' => 200001, 'capacity' => 30],
                ['id' => '7dae143a-ac48-444c-8024-27487aa535e8', 'group_number' => 200002, 'capacity' => 30],
                ['id' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'group_number' => 300001, 'capacity' => 30],
                ['id' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'group_number' => 300002, 'capacity' => 30],
                ['id' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'group_number' => 400001, 'capacity' => 40],
                ['id' => 'c12124f3-52a8-4429-bdd9-660c6df2eedc', 'group_number' => 500100, 'capacity' => 20],
                ['id' => 'f064c210-2673-4851-a2c7-e758bdf23239', 'group_number' => 600100, 'capacity' => 60],
            ]
        );
    }
}

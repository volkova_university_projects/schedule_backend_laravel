<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            ClassroomSeeder::class,
            GroupSeeder::class,
            StudentSeeder::class,
            TimeslotSeeder::class,
            SubjectSeeder::class,
            TeacherSeeder::class,
            LessonSeeder::class,
            ScheduleSeeder::class,
            GroupOnLessonSeeder::class,
            TimeAndPlaceLessonSeeder::class
        ]);
//        $this->call([
//            GenreSeeder::class,
//            MovieSeeder::class,
//            MovieGenreSeeder::class,
//            UserSeeder::class,
//            ProfileSeeder::class,
//            ReviewSeeder::class,
//        ]);
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}

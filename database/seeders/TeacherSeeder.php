<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teachers')->insert(
            [
                ['id' => 'bb6f4d95-8559-4598-a512-9223b6de9368', 'user_id' => 'e082d25b-16f4-469e-8591-2fea1e5cbfe3'],
                ['id' => 'd60ca909-eaee-473d-b3d0-951913fecf9b', 'user_id' => 'e6d7b35b-c9f0-4a9c-ab6a-e07504a8ac4f'],
                ['id' => '967e3b2a-278f-424d-9ec5-eb88c958e8eb', 'user_id' => 'e74931df-494b-4663-a75f-d0bd6cc8cac6'],
                ['id' => '73afc893-9fd2-45f9-b9f0-9ca2700208fa', 'user_id' => 'f955246f-0fd9-4482-9c96-28e7f9a96274'],
                ['id' => '802bcc92-ee97-414f-b95f-b8e3d6b75fd7', 'user_id' => 'fcf6c8c2-e3c4-42d6-995a-43d666aaad16'],
                ['id' => 'fe637274-2533-4dd6-aa91-2179dc68bd44', 'user_id' => '004ae6fd-cf8b-4852-be30-fcacc6ef5a39'],
                ['id' => '63426967-3dc9-41e1-b1ea-6ea31456d00e', 'user_id' => '03c857af-4d4a-4f9d-87e4-32e0307ac7b5'],
            ]
        );
    }
}

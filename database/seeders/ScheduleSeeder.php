<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedulers')->insert(
            [
                ['id' => '620baade-53b1-4f23-b9ad-40d65332f304', 'user_id' => '004ae6fd-cf8b-4852-be30-fcacc6ef5a39'],
                ['id' => 'e77fc5e9-c768-487b-88fb-edc814c39008', 'user_id' => '03c857af-4d4a-4f9d-87e4-32e0307ac7b5'],
                ['id' => 'b2c9a7fc-46ae-4d95-85d3-1ae58b382b82', 'user_id' => '06b1ef64-3a2e-4d82-812b-ab0486387e3b'],
                ['id' => 'a4fb7286-436f-4ddf-bdda-202db20daf72', 'user_id' => '0c5a8899-04bb-40e1-936b-36473e91a8ec'],
                ['id' => '7ffe77f5-4fd4-4795-9e05-280ccc7e73ed', 'user_id' => '0f648907-5a81-4d19-9ed3-da3672ad8ac9'],
                ['id' => '45c6f0c7-b7d1-44c4-adbb-396767c4c336', 'user_id' => '133a64b4-99a9-4a2a-9635-b4d4cafa6cb3'],
                ['id' => 'b35f26ef-28b2-4ece-b9b4-7d0bfd3e1648', 'user_id' => '20780381-c3ba-4051-9069-d07f4509841b'],
                ['id' => '959bbbf6-d53e-4c45-97d0-af41e120af80', 'user_id' => '29a5834f-d036-4b01-aee3-d1b31dcfec85'],
            ]
        );
    }
}

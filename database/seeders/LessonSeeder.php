<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lessons')->insert(
            [
                ['id' => '48339866-c09a-4b41-8098-293eff6b975c', 'subject' => '017b3252-30bc-4891-8602-84d8b38f8538', 'teacher' => '967e3b2a-278f-424d-9ec5-eb88c958e8eb', 'type_of_lesson' => 'Лекция'],
                ['id' => 'adb5cccc-82a4-4013-904b-be11797bd872', 'subject' => '017b3252-30bc-4891-8602-84d8b38f8538', 'teacher' => '73afc893-9fd2-45f9-b9f0-9ca2700208fa', 'type_of_lesson' => 'Практика'],
                ['id' => 'cb1b8afa-a790-4dc4-b72f-ee233b2b53bb', 'subject' => '0b26efc3-64ee-441d-9885-48a5bd73a1a2', 'teacher' => 'fe637274-2533-4dd6-aa91-2179dc68bd44', 'type_of_lesson' => 'Лекция'],
                ['id' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'subject' => '0b26efc3-64ee-441d-9885-48a5bd73a1a2', 'teacher' => '63426967-3dc9-41e1-b1ea-6ea31456d00e', 'type_of_lesson' => 'Практика'],
                ['id' => '528defe8-6be2-4812-bafd-d74d11d11550', 'subject' => '2527fb77-2a4d-42db-9ff4-a9d1b4d9b91e', 'teacher' => '802bcc92-ee97-414f-b95f-b8e3d6b75fd7', 'type_of_lesson' => 'Лекция'],
                ['id' => '381e1471-c131-4259-9ea1-e310d8182575', 'subject' => '51cbb285-e971-4ebe-9dbf-9b18734d1dcf', 'teacher' => 'bb6f4d95-8559-4598-a512-9223b6de9368', 'type_of_lesson' => 'Лекция'],
                ['id' => '01f11d6f-4d43-444b-8b88-442220149672', 'subject' => '5610a04c-26e0-4e12-a246-d1ba61381577', 'teacher' => '967e3b2a-278f-424d-9ec5-eb88c958e8eb', 'type_of_lesson' => 'Лекция'],
                ['id' => '562ee073-6161-4957-ba1a-3c508501d94c', 'subject' => '6f822ee0-2160-4fd9-93e9-f672806d4c72', 'teacher' => '802bcc92-ee97-414f-b95f-b8e3d6b75fd7', 'type_of_lesson' => 'Лекция'],
                ['id' => '53e72adc-a886-4180-b208-5e5612e07447', 'subject' => '6f822ee0-2160-4fd9-93e9-f672806d4c72', 'teacher' => 'd60ca909-eaee-473d-b3d0-951913fecf9b', 'type_of_lesson' => 'Пратика'],
                ['id' => '9ee618dc-0e95-4962-8e63-6327a4fbb6e8', 'subject' => '80154d1a-f1c4-400d-b72b-c3f24a0cb18c', 'teacher' => 'd60ca909-eaee-473d-b3d0-951913fecf9b', 'type_of_lesson' => 'Практика'],
                ['id' => 'a491a401-2f6a-45dd-9f09-4ae34878dac5', 'subject' => '8785dbab-11fb-407d-9082-932aae6a1edf', 'teacher' => 'bb6f4d95-8559-4598-a512-9223b6de9368', 'type_of_lesson' => 'Лекция'],
                ['id' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'subject' => '8785dbab-11fb-407d-9082-932aae6a1edf', 'teacher' => '802bcc92-ee97-414f-b95f-b8e3d6b75fd7', 'type_of_lesson' => 'Практика'],
                ['id' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'subject' => '92641936-83ee-4670-a0db-256b9fdd7b41', 'teacher' => 'd60ca909-eaee-473d-b3d0-951913fecf9b', 'type_of_lesson' => 'Семинар'],
                ['id' => 'd6a43946-80fb-4105-bd1f-bbfcfc503ccc', 'subject' => '92641936-83ee-4670-a0db-256b9fdd7b41', 'teacher' => '967e3b2a-278f-424d-9ec5-eb88c958e8eb', 'type_of_lesson' => 'Семинар'],
                ['id' => 'e2fa57fc-182e-46c8-b8fa-f15f14c8dbd6', 'subject' => 'a59beaec-070c-4c47-b8d9-8ed1c3af83e8', 'teacher' => '967e3b2a-278f-424d-9ec5-eb88c958e8eb', 'type_of_lesson' => 'Семинар'],
            ]
        );
    }
}

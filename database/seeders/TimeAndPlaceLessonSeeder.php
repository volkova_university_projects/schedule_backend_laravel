<?php

namespace Database\Seeders;

use Carbon\CarbonImmutable;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class TimeAndPlaceLessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $startOfWeek = CarbonImmutable::now()->startOfWeek();

        DB::table('time_and_place_lessons')->insert(
            [
                ['id' => 'ad27f689-a049-40d7-b565-ddd59486f548', 'classroom' => '030acae5-d27f-4f55-8dc9-d0ce794468ce', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => 'bbdc64f5-b948-4c33-bda8-c07f63b12ae6', 'classroom' => '09d97959-056f-4293-83f3-2f67af262036', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => '1bd222e8-c74c-4241-9115-f14ee972ef90', 'classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => '88dc7d4c-bf29-4c36-aa36-c2857f8a9d14', 'classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => 'dafcb63c-bbf3-4e41-a474-74c466435636', 'classroom' => '2aff3fb1-bcd8-46f2-9a61-4e5eedb2a8cb', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => '8cec5005-4acb-4d72-81ac-4780bcb01020', 'classroom' => '49e92242-177b-40d4-8cb4-6cb0874e75df', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => '6c0b572a-b82e-4044-8291-e7ddd3e895d5', 'classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => '206f4d8d-e2cf-44a8-9904-d8153e8ca822', 'classroom' => '52724c3e-536b-4bbb-8c4b-61a176ca6af7', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => '54ba9c59-6cea-4650-8d6b-8278a967ed7f', 'classroom' => '559def39-80ae-41d7-b0ec-abeb756b1a3b', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => 'faae9b59-e726-425f-8a1a-4c59f0179249', 'classroom' => '71487dbb-b773-4c41-98f9-f1ee124a88da', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => 'a64931b9-695d-49ee-abdb-9a4a5e5ac787', 'classroom' => '7199e78e-ad24-4873-b5ed-10dfe1d166a8', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => 'd6a43946-80fb-4105-bd1f-bbfcfc503ccc', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => 'f45427d3-cd15-4a1f-8b68-04309ef9a7d7', 'classroom' => '72ded9e7-d10c-46e7-986d-2f4a2308cd89', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => 'f9c9e34d-8101-4427-b45d-fe467f3dbfa2', 'classroom' => '7c895e7e-d605-4ecc-970a-9094c1614689', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $startOfWeek->format('Y-m-d')],
                ['id' => 'e27003f9-4589-49fd-a45d-3793ead82495', 'classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => 'd1dfacbc-fba7-4e88-8ad0-24468910b441', 'classroom' => '52724c3e-536b-4bbb-8c4b-61a176ca6af7', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '9efd3256-8094-4430-b7d7-6b2fa437b234', 'classroom' => '559def39-80ae-41d7-b0ec-abeb756b1a3b', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '513d4f41-0b32-40f9-9846-6f870731da19', 'classroom' => '030acae5-d27f-4f55-8dc9-d0ce794468ce', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '9b2157d1-9f13-47a7-ae1c-604f41cbe6f9', 'classroom' => '09d97959-056f-4293-83f3-2f67af262036', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '7f6103ca-a80b-45af-9a44-4b01a22dbe96', 'classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '186cf64c-5de7-4014-867a-0473f7348e9d', 'classroom' => '2aff3fb1-bcd8-46f2-9a61-4e5eedb2a8cb', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '12198084-d658-473b-b626-67e7e337198b', 'classroom' => '49e92242-177b-40d4-8cb4-6cb0874e75df', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '58525359-11df-45ae-ab49-375254f216a4', 'classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '5cf1d642-15c9-4380-a7df-5ed2b97b8392', 'classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '6eaea747-1b0a-430b-8b5e-1fab22276a34', 'classroom' => '2aff3fb1-bcd8-46f2-9a61-4e5eedb2a8cb', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => 'd2a667a8-999d-4d4f-945d-a60302017a4f', 'classroom' => '49e92242-177b-40d4-8cb4-6cb0874e75df', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $startOfWeek->addDays(1)->format('Y-m-d')],
                ['id' => '9d5a81e0-3da1-4c72-8564-f3878f2b6bb5', 'classroom' => '52724c3e-536b-4bbb-8c4b-61a176ca6af7', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '98120158-289c-43f4-9c39-97e917909ee7', 'classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => 'e7a2fd03-bb0f-4954-bc35-e39f16807a50', 'classroom' => '559def39-80ae-41d7-b0ec-abeb756b1a3b', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => '7dae143a-ac48-444c-8024-27487aa535e8', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '2c625bd0-111c-4054-908f-b106d6f823b0', 'classroom' => '71487dbb-b773-4c41-98f9-f1ee124a88da', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => '7dae143a-ac48-444c-8024-27487aa535e8', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '140c9d7e-f620-4884-be7e-9d7ebfe00287', 'classroom' => '7199e78e-ad24-4873-b5ed-10dfe1d166a8', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => '7dae143a-ac48-444c-8024-27487aa535e8', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '333034da-5c56-4775-980d-002640dc20f0', 'classroom' => '72ded9e7-d10c-46e7-986d-2f4a2308cd89', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '77541753-1339-488b-af61-c19fe975a051', 'classroom' => '030acae5-d27f-4f55-8dc9-d0ce794468ce', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '13161f25-dd5a-43b8-8090-ba1a09322bd6', 'classroom' => '09d97959-056f-4293-83f3-2f67af262036', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => 'c62f2dc0-027e-46b6-8218-d55c5a5ac7c8', 'classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '9dd70e5a-6d2e-4c12-9327-da4b3fc28848', 'classroom' => '49e92242-177b-40d4-8cb4-6cb0874e75df', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '289dfcad-46c1-4580-a0d4-6306a07427d2', 'classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => 'd33bd767-f9ba-497b-8dbc-264590239a86', 'classroom' => '2aff3fb1-bcd8-46f2-9a61-4e5eedb2a8cb', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '251d5d45-0556-4b34-89cc-7e67d9923c88', 'classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => '1219cabc-5fcd-45b0-9b95-c64a7501d2f2', 'classroom' => '52724c3e-536b-4bbb-8c4b-61a176ca6af7', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
                ['id' => 'ef4d5a1e-6b04-4222-b671-595a49e502c2', 'classroom' => '030acae5-d27f-4f55-8dc9-d0ce794468ce', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $startOfWeek->addDays(2)->format('Y-m-d')],
            ]

        );

        for($i = -2; $i < 3; $i++){
            if ($i < 0){
                $thisWeek = $startOfWeek->subWeeks(abs($i));
            } elseif ($i > 0) {
                $thisWeek = $startOfWeek->addWeeks($i);
            } else {
                continue;
            }
            DB::table('time_and_place_lessons')->insert(
                [
                    ['classroom' => '030acae5-d27f-4f55-8dc9-d0ce794468ce', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '09d97959-056f-4293-83f3-2f67af262036', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '2aff3fb1-bcd8-46f2-9a61-4e5eedb2a8cb', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '49e92242-177b-40d4-8cb4-6cb0874e75df', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '52724c3e-536b-4bbb-8c4b-61a176ca6af7', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '559def39-80ae-41d7-b0ec-abeb756b1a3b', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '71487dbb-b773-4c41-98f9-f1ee124a88da', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '7199e78e-ad24-4873-b5ed-10dfe1d166a8', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => 'd6a43946-80fb-4105-bd1f-bbfcfc503ccc', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '72ded9e7-d10c-46e7-986d-2f4a2308cd89', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '7c895e7e-d605-4ecc-970a-9094c1614689', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $thisWeek->format('Y-m-d')],
                    ['classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '52724c3e-536b-4bbb-8c4b-61a176ca6af7', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '559def39-80ae-41d7-b0ec-abeb756b1a3b', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'f835481b-b959-4f25-846e-a8ac50b7c62d', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '030acae5-d27f-4f55-8dc9-d0ce794468ce', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '09d97959-056f-4293-83f3-2f67af262036', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '2aff3fb1-bcd8-46f2-9a61-4e5eedb2a8cb', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '49e92242-177b-40d4-8cb4-6cb0874e75df', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'ca6e2c79-9420-4911-8991-bdff228270ef', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '2aff3fb1-bcd8-46f2-9a61-4e5eedb2a8cb', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '49e92242-177b-40d4-8cb4-6cb0874e75df', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $thisWeek->addDays(1)->format('Y-m-d')],
                    ['classroom' => '52724c3e-536b-4bbb-8c4b-61a176ca6af7', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '559def39-80ae-41d7-b0ec-abeb756b1a3b', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => '7dae143a-ac48-444c-8024-27487aa535e8', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '71487dbb-b773-4c41-98f9-f1ee124a88da', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => '7dae143a-ac48-444c-8024-27487aa535e8', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '7199e78e-ad24-4873-b5ed-10dfe1d166a8', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => '7dae143a-ac48-444c-8024-27487aa535e8', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '72ded9e7-d10c-46e7-986d-2f4a2308cd89', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '030acae5-d27f-4f55-8dc9-d0ce794468ce', 'timeslot' => 'd3d56740-ea58-4629-a513-e28ccf09b864', 'lesson' => 'ed5299fe-8375-45bf-bfea-b1453b4631b2', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '09d97959-056f-4293-83f3-2f67af262036', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '48339866-c09a-4b41-8098-293eff6b975c', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '49e92242-177b-40d4-8cb4-6cb0874e75df', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '0bc4011a-acf5-4ed9-a2d5-e22ce269d200', 'timeslot' => '64d98b9f-cdb9-4e28-8d9c-92b965bf41ba', 'lesson' => '01f11d6f-4d43-444b-8b88-442220149672', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '2aff3fb1-bcd8-46f2-9a61-4e5eedb2a8cb', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '28ec9b0b-e771-46d3-8215-ec4e57e3995e', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '4a4f135f-a69a-41ad-9229-5b9acbf1b507', 'timeslot' => 'ffe5ea80-c053-44a4-bb56-5ec662887768', 'lesson' => '45ac8c10-afaa-43a8-ba7a-20fcb9b0255a', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '52724c3e-536b-4bbb-8c4b-61a176ca6af7', 'timeslot' => 'ae2017be-a02f-48c9-b0e9-c27d19ccbac6', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                    ['classroom' => '030acae5-d27f-4f55-8dc9-d0ce794468ce', 'timeslot' => 'faec1a7e-ac08-4dce-9f85-5b25028db059', 'lesson' => '381e1471-c131-4259-9ea1-e310d8182575', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'date' => $thisWeek->addDays(2)->format('Y-m-d')],
                ]

            );

        }

    }
}

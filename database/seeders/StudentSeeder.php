<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert(
            [
                ['id' => '258bf19a-529f-4d3f-b6c7-96a7ae86aba6', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'user_id' => '004ae6fd-cf8b-4852-be30-fcacc6ef5a39'],
                ['id' => '81bf5bcf-4555-4b60-95d1-793421ca620a', 'group' => '7dae143a-ac48-444c-8024-27487aa535e8', 'user_id' => '03c857af-4d4a-4f9d-87e4-32e0307ac7b5'],
                ['id' => '8fc9e9b1-1e8f-4e12-9719-1d1843bfa390', 'group' => null, 'user_id' => '06b1ef64-3a2e-4d82-812b-ab0486387e3b'],
                ['id' => '73fe0d9c-278b-4740-bf3a-1c32c1ce4a8d', 'group' => null, 'user_id' => '0c5a8899-04bb-40e1-936b-36473e91a8ec'],
                ['id' => '2442fae0-2bdd-4ecd-8aaf-89c2edefb661', 'group' => '7dae143a-ac48-444c-8024-27487aa535e8', 'user_id' => '0f648907-5a81-4d19-9ed3-da3672ad8ac9'],
                ['id' => '83d2f268-0f9a-4c79-a0f7-235ecb3f92c4', 'group' => null, 'user_id' => '133a64b4-99a9-4a2a-9635-b4d4cafa6cb3'],
                ['id' => '71056459-7ca1-439a-891a-ab715fdf600c', 'group' => 'f064c210-2673-4851-a2c7-e758bdf23239', 'user_id' => '20780381-c3ba-4051-9069-d07f4509841b'],
                ['id' => '6ba80bbb-f8ad-40f2-9acc-ea07f6eb4b07', 'group' => 'fb240897-22ed-4abf-96ab-38ec9322fe60', 'user_id' => '29a5834f-d036-4b01-aee3-d1b31dcfec85'],
                ['id' => 'a52738cb-abeb-4dd3-9cfc-0cada8c85825', 'group' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'user_id' => '389548dc-ab95-443b-a6bc-5ec2bc7b3989'],
                ['id' => 'c90b49a2-6673-4fc1-9c55-b5e24176775f', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'user_id' => '3cef2af3-1923-49d9-962f-5446d7e0175e'],
                ['id' => '49d690e2-66a9-42f2-a903-0268d0dc844b', 'group' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'user_id' => '3e9fb3a1-7bee-4f5d-a4ad-41ccb5f0b843'],
                ['id' => '4f649ad3-fe27-40b1-b2a7-a6a0cba2e660', 'group' => 'c12124f3-52a8-4429-bdd9-660c6df2eedc', 'user_id' => '4386c7ee-7151-441e-8bbf-78fc2ec31f90'],
                ['id' => '59ab79e7-04d9-414b-9b05-5cdce11ef1b6', 'group' => 'c12124f3-52a8-4429-bdd9-660c6df2eedc', 'user_id' => '4a75f8b5-0e13-42e3-8738-70fd84a0c16a'],
                ['id' => '75dd98c7-025e-4fab-b5b7-7eb989cff376', 'group' => 'c12124f3-52a8-4429-bdd9-660c6df2eedc', 'user_id' => '4f397196-1c8e-4622-b04f-b76da09fa9f7'],
                ['id' => '5899c660-c213-4d4d-ae9b-b684902efb6e', 'group' => 'c12124f3-52a8-4429-bdd9-660c6df2eedc', 'user_id' => '558d2fcb-a4fd-4c41-b768-93860d7895bc'],
                ['id' => '65904630-8810-4f0b-a116-d0d92dd5946b', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'user_id' => '652900b1-d816-4ac5-b87b-a60a5e8c10df'],
                ['id' => '27e3018b-8bd6-4673-9e19-549ddc047c9d', 'group' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'user_id' => '6d0f6184-921e-4d95-aa02-59a581ec0e39'],
                ['id' => '761425ca-f9f8-4f5d-8802-d4dc25e1ad47', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'user_id' => '714f4ad6-4051-4fd5-b427-91622b2ba9c2'],
                ['id' => 'e9f48489-d8d7-46a2-b531-1037adcd6242', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'user_id' => '7209337d-49bf-4d83-80d0-709bb2c2a207'],
                ['id' => '1f5e39e8-5e8a-474b-bd1b-86493808a0c8', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'user_id' => '7f89b0f4-62f6-4e90-abd9-422ff9146347'],
                ['id' => '8831c26c-6aba-4c24-b6e6-4a453998bf10', 'group' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'user_id' => '8f68d7c1-3905-4099-bc5e-24014d59e2c1'],
                ['id' => '4d0823ee-3e8a-4927-a337-f1ace3b8d61d', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'user_id' => '8f93b71b-76b6-4dfd-b290-b4bd6b66888b'],
                ['id' => 'f3066e15-3add-4ba6-ab3f-82981b26a586', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'user_id' => '9836532e-a657-437c-8bf3-35f17e9091b0'],
                ['id' => 'b87b6a78-2e5b-419d-9ab2-1c1ac8fa5ca9', 'group' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'user_id' => '9a1bf7c9-8146-4da9-a638-30ef826932c5'],
                ['id' => '750bdb68-a0e0-4f23-9f98-a93115ea2a95', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'user_id' => '9cf583df-7815-44cf-b2d9-3bd22add63c2'],
                ['id' => '490a2aa2-f32e-446c-a298-fd3f567e9a6b', 'group' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'user_id' => 'a25527f5-2dbe-4d39-9669-7edeaec594f7'],
                ['id' => '62731ee4-e1f6-47b6-8d52-e92b267baf79', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'user_id' => 'a62f2a56-3202-483f-acfe-296a1716aa74'],
                ['id' => '85dc420e-53d3-49ef-b7f5-929b3d2f51f6', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'user_id' => 'a917a7c2-7895-42ce-922a-a1104b2ffb21'],
                ['id' => 'c708a34a-11e7-477d-8cfa-8cbbdd5b2508', 'group' => 'f064c210-2673-4851-a2c7-e758bdf23239', 'user_id' => 'ae919f76-ec11-4b54-ba8b-d104a0e2b1fb'],
                ['id' => '5be846b6-6243-4ef4-b0dc-6611286b4ed8', 'group' => '433fd62f-e6a7-4c77-8376-c1b43526662e', 'user_id' => 'b178ac9d-078f-447a-a529-53285e659051'],
                ['id' => 'ec64d9a7-fb90-4025-a51e-104c9d6dfeb7', 'group' => 'e9ac4335-5571-4c6e-9c27-e1c30a58d18f', 'user_id' => 'c3aefc8a-bd8a-4dd6-9c3b-1cf9975fc698'],
                ['id' => '1388ef37-d6ab-45db-9e17-645a408e2809', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'user_id' => 'c74ab626-2d8a-463f-8a2e-4af7ba67eb6c'],
                ['id' => '1dc37332-fd3c-4fd4-927f-d2e729359d99', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'user_id' => 'cc4c826f-9744-4ccf-9136-986b558fb3f1'],
                ['id' => '0c4656d1-5154-4ae3-b471-7ac12abe7f65', 'group' => 'c9802e13-57c2-4322-acbe-278050376b46', 'user_id' => 'd2bc9513-d36f-4412-a5da-4fdeaa057e6a'],
                ['id' => 'e8f003b9-c5fe-487b-b531-6b2b1d8a43e6', 'group' => 'd50281ac-586f-4349-95be-96b57fbe48ce', 'user_id' => 'dc73fcd6-d8a2-4866-8280-edd9acef7951'],
                ['id' => '227af8b0-e0f0-42fa-bdba-fb81cc271881', 'group' => null, 'user_id' => 'e082d25b-16f4-469e-8591-2fea1e5cbfe3'],
                ['id' => '3a0b3339-bd9e-4f57-b757-d686e6ada3b0', 'group' => null, 'user_id' => 'e6d7b35b-c9f0-4a9c-ab6a-e07504a8ac4f'],
                ['id' => 'b3212c10-7362-4769-9edb-c0b21c2c950d', 'group' => null, 'user_id' => 'e74931df-494b-4663-a75f-d0bd6cc8cac6'],
                ['id' => '8eef02f1-180a-4765-aad5-b43cfada6793', 'group' => null, 'user_id' => 'f955246f-0fd9-4482-9c96-28e7f9a96274'],
                ['id' => 'bf76a2b6-b37f-42ee-a182-3812f42cc4f6', 'group' => null, 'user_id' => 'fcf6c8c2-e3c4-42d6-995a-43d666aaad16'],
            ]
        );
    }
}

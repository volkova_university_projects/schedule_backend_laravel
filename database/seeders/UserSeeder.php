<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                ['id' => 'c74ab626-2d8a-463f-8a2e-4af7ba67eb6c', 'userName' => 'Тихонова Вероника Лукинична', 'email' => 'Lawen1984@dayrep.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '3cef2af3-1923-49d9-962f-5446d7e0175e', 'userName' => 'Лавров Максим Богданович', 'email' => 'casuquoussumau-7709@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'a62f2a56-3202-483f-acfe-296a1716aa74', 'userName' => 'Устинов Демид Денисович', 'email' => 'govejabeizu-9384@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'cc4c826f-9744-4ccf-9136-986b558fb3f1', 'userName' => 'Жаров Марк Дмитриевич', 'email' => 'pratuximaffu-3137@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'fcf6c8c2-e3c4-42d6-995a-43d666aaad16', 'userName' => 'Поляков Михаил Михайлович', 'email' => 'cafivofropreu-3107@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '652900b1-d816-4ac5-b87b-a60a5e8c10df', 'userName' => 'Данилова Анна Петровна', 'email' => 'tranefrafoudau-6119@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '133a64b4-99a9-4a2a-9635-b4d4cafa6cb3', 'userName' => 'Третьяков Арсений Михайлович', 'email' => 'ropautreiffeuxe-9262@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '0c5a8899-04bb-40e1-936b-36473e91a8ec', 'userName' => 'Шишкина Владислава Михайловна', 'email' => 'geuqueimekeiti-2201@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'c3aefc8a-bd8a-4dd6-9c3b-1cf9975fc698', 'userName' => 'Александрова Таисия Егоровна', 'email' => 'bruproujollake-6037@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '6d0f6184-921e-4d95-aa02-59a581ec0e39', 'userName' => 'Данилов Рустам Тимофеевич', 'email' => 'dazitoipparau-6746@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '03c857af-4d4a-4f9d-87e4-32e0307ac7b5', 'userName' => 'Киселева Ксения Ильинична', 'email' => 'fanneufitipra-3840@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'a917a7c2-7895-42ce-922a-a1104b2ffb21', 'userName' => 'Иванов Владимир Павлович', 'email' => 'noilligottipou-3423@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '0f648907-5a81-4d19-9ed3-da3672ad8ac9', 'userName' => 'Морозова Милана Тимофеевна', 'email' => 'loijixougugra-7470@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '06b1ef64-3a2e-4d82-812b-ab0486387e3b', 'userName' => 'Волкова Кира Ильинична', 'email' => 'niffifrehifroi-7026@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '9a1bf7c9-8146-4da9-a638-30ef826932c5', 'userName' => 'Давыдов Платон Григорьевич', 'email' => 'greihoinnacacre-5314@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '4f397196-1c8e-4622-b04f-b76da09fa9f7', 'userName' => 'Бабушкин Марк Маркович', 'email' => 'brammefaffammi-5229@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '4a75f8b5-0e13-42e3-8738-70fd84a0c16a', 'userName' => 'Бирюков Никита Арсентьевич', 'email' => 'tronnuyatteubru-8306@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '4386c7ee-7151-441e-8bbf-78fc2ec31f90', 'userName' => 'Тихонов Роман Тимурович', 'email' => 'loppinajaunau-7027@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'e082d25b-16f4-469e-8591-2fea1e5cbfe3', 'userName' => 'Бобров Руслан Арсентьевич', 'email' => 'denellamutto-9801@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '8f93b71b-76b6-4dfd-b290-b4bd6b66888b', 'userName' => 'Ковалев Артём Фёдорович', 'email' => 'raulucrequesu-9277@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'dc73fcd6-d8a2-4866-8280-edd9acef7951', 'userName' => 'Лобанова Милана Макаровна', 'email' => 'nuhiddeicazo-7806@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '7f89b0f4-62f6-4e90-abd9-422ff9146347', 'userName' => 'Самсонов Макар Матвеевич', 'email' => 'pucovodeuquoi-7158@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'f955246f-0fd9-4482-9c96-28e7f9a96274', 'userName' => 'Горохова Дарья Макаровна', 'email' => 'trauweupoippefeu-8373@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '558d2fcb-a4fd-4c41-b768-93860d7895bc', 'userName' => 'Фирсов Игорь Никитич', 'email' => 'bucrottocoigou-8410@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'd2bc9513-d36f-4412-a5da-4fdeaa057e6a', 'userName' => 'Ильина Милана Матвеевна', 'email' => 'hubeigreumouttou-8139@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '9836532e-a657-437c-8bf3-35f17e9091b0', 'userName' => 'Измайлова Виктория Егоровна', 'email' => 'brammoideuxautti-8696@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '8f68d7c1-3905-4099-bc5e-24014d59e2c1', 'userName' => 'Иванов Михаил Павлович', 'email' => 'balloifraunaffa-8403@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'b178ac9d-078f-447a-a529-53285e659051', 'userName' => 'Кузнецова Елена Егоровна', 'email' => 'leummauttoucroicre-7316@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '3e9fb3a1-7bee-4f5d-a4ad-41ccb5f0b843', 'userName' => 'Дружинин Борис Леонович', 'email' => 'truwelillose-1412@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '29a5834f-d036-4b01-aee3-d1b31dcfec85', 'userName' => 'Ульянов Фёдор Святославович', 'email' => 'kuxovaugako-6109@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '004ae6fd-cf8b-4852-be30-fcacc6ef5a39', 'userName' => 'Соловьев Ибрагим Георгиевич', 'email' => 'freubriwumeije-1279@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'e6d7b35b-c9f0-4a9c-ab6a-e07504a8ac4f', 'userName' => 'Попова Виктория Вячеславовна', 'email' => 'pokorocrorei-2440@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '20780381-c3ba-4051-9069-d07f4509841b', 'userName' => 'Маркина Софья Владимировна', 'email' => 'xaukeicecrautra-7597@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '7209337d-49bf-4d83-80d0-709bb2c2a207', 'userName' => 'Рудакова София Романовна', 'email' => 'hopeumeiducre-5284@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '389548dc-ab95-443b-a6bc-5ec2bc7b3989', 'userName' => 'Колесова Мария Викторовна', 'email' => 'baujunnollappou-4361@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '714f4ad6-4051-4fd5-b427-91622b2ba9c2', 'userName' => 'Сальников Артём Русланович', 'email' => 'neilluvocicreu-6351@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => '9cf583df-7815-44cf-b2d9-3bd22add63c2', 'userName' => 'Кондратьева Вероника Степановна', 'email' => 'quisseifrecrugre-8401@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'e74931df-494b-4663-a75f-d0bd6cc8cac6', 'userName' => 'Петров Тимофей Богданович', 'email' => 'bilepocroimmoi-3058@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'a25527f5-2dbe-4d39-9669-7edeaec594f7', 'userName' => 'Смирнов Александр Константинович', 'email' => 'hiquettounnitoi-5672@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
                ['id' => 'ae919f76-ec11-4b54-ba8b-d104a0e2b1fb', 'userName' => 'Зимин Дмитрий Даниэльевич', 'email' => 'xoufrejannoisseu-6415@yopmail.com', 'password' => '$2y$10$OpIN9krfcM5.0HBD1AmXV.ryyvbtVb/aTBffqC4qruNb8JNuxo0KW'],
            ]
        );
    }
}

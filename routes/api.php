<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ClassroomController;
use App\Http\Controllers\Api\GroupController;
use App\Http\Controllers\Api\LessonTemplatesController;
use App\Http\Controllers\Api\ScheduleLessonsController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\SchedulerController;
use App\Http\Controllers\Api\StudentController;
use App\Http\Controllers\Api\SubjectController;
use App\Http\Controllers\Api\TeacherController;
use App\Http\Controllers\Api\TimeslotController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->group(function () {
   //здесь действия только для авторизованных пользователей
    Route::post('/account/logout', [AuthController::class, 'logoutUser']);
    Route::get('/account/profile', [ProfileController::class, 'show']);
    Route::put('/account/profile', [ProfileController::class, 'update']);

    Route::get('/students', [StudentController::class, 'index']);
    Route::get('/schedulers', [SchedulerController::class, 'index']);

    Route::controller(LessonTemplatesController::class)->group(function () {
        Route::get('/lessons', 'index');
        Route::post('/lessons', 'createTemplate');
        //with template's id
        Route::put('/lessons/{id}', 'updateTemplate');
        Route::delete('/lessons/{id}', 'deleteTemplate');
    });

    Route::controller(ScheduleLessonsController::class)->group(function () {
        Route::post('/schedule', 'create');
        //with lesson's id
        Route::put('/schedule/{id}', 'updateLesson');
        Route::delete('/schedule/{id}', 'deleteLesson');
    });


    Route::controller(SubjectController::class)->group(function () {
        Route::get('/subjects', 'index');
        Route::post('/subjects', 'create');
        Route::put('/subjects/{id}', 'update');
        Route::delete('/subjects/{id}', 'destroy');
    });

    Route::controller(UserController::class)->group(function () {
        Route::get('/users', 'index');
        Route::post('/users', 'create');
        Route::put('/users/{id}', 'update');
        Route::delete('/users/{id}', 'destroy');
    });

    Route::controller(ClassroomController::class)->group(function () {
        Route::post('/classrooms', 'create');
        Route::put('/classrooms/{id}', 'update');
        Route::delete('/classrooms/{id}', 'destroy');

    });
    Route::controller(GroupController::class)->group(function () {
        Route::post('/groups', 'create');
        Route::put('/groups/{id}', 'update');
        Route::get('/groups/{id}/users', 'indexUsers');
        Route::post('/groups/{id}/users', 'createUser');
        Route::delete('/groups/{id}/users/{idUser}', 'destroyUser');
    });



});

//ниже действия для неавторизованных пользователей

Route::controller(AuthController::class)->group(function () {
    Route::post('/account/login', 'loginUser');
    Route::post('/account/register', 'createUser');
});

//болванки
Route::get('/timeslots', [TimeslotController::class, 'index']);
Route::get('/teachers', [TeacherController::class, 'index']);
Route::get('/groups', [GroupController::class, 'index']);
Route::get('/classrooms', [ClassroomController::class, 'index']);


//расписания
Route::get('/schedule/teacher/{id}', [TeacherController::class, 'indexSchedule']);
Route::get('/schedule/group/{id}', [GroupController::class, 'indexSchedule']);
Route::get('/schedule/classroom/{id}', [ClassroomController::class, 'indexSchedule']);

Route::get('/schedule', [ScheduleLessonsController::class, 'index']);
Route::get('/schedule/{id}', [ScheduleLessonsController::class, 'showLesson']);


